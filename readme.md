Exemple de custom visual de barChart responsif sous React + plotly. 

Documentation de plotly react accessible [ici]

## Documentation plotly React
La documentation de plotly react est accessible [ici](https://github.com/plotly/react-plotly.js) 

### Exemples de config

Des exemples de config sont disponibles [ici](https://plot.ly/javascript/configuration-options)