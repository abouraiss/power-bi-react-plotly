import { Visual } from "../../src/visual";
import powerbiVisualsApi from "powerbi-visuals-api"
import IVisualPlugin = powerbiVisualsApi.visuals.plugins.IVisualPlugin
import VisualConstructorOptions = powerbiVisualsApi.extensibility.visual.VisualConstructorOptions
var powerbiKey: any = "powerbi";
var powerbi: any = window[powerbiKey];

var aqslinechartt00320AAC82F54F798A68F1F888194018: IVisualPlugin = {
    name: 'aqslinechartt00320AAC82F54F798A68F1F888194018',
    displayName: 'AQS LineChart',
    class: 'Visual',
    apiVersion: '2.6.0',
    create: (options?: VisualConstructorOptions) => {
        if (Visual) {
            return new Visual(options);
        }

        throw 'Visual instance not found';
    },
    custom: true
};

if (typeof powerbi !== "undefined") {
    powerbi.visuals = powerbi.visuals || {};
    powerbi.visuals.plugins = powerbi.visuals.plugins || {};
    powerbi.visuals.plugins["aqslinechartt00320AAC82F54F798A68F1F888194018"] = aqslinechartt00320AAC82F54F798A68F1F888194018;
}

export default aqslinechartt00320AAC82F54F798A68F1F888194018;