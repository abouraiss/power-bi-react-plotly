function logExceptions(): MethodDecorator {

  return function (target: Object, propertyKey: string, descriptor: TypedPropertyDescriptor<any>): TypedPropertyDescriptor<any> {

      return {
          value: function () {

              try {
                  return descriptor.value.apply(this, arguments);
              } catch (e) {
                  console.error(e);
                  throw e;
              }
          }
      }
  }

}
export default logExceptions;