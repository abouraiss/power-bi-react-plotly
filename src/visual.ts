"use strict";
import "@babel/polyfill";
import powerbi from "powerbi-visuals-api";

import DataView = powerbi.DataView;
import VisualConstructorOptions = powerbi.extensibility.visual.VisualConstructorOptions;
import VisualUpdateOptions = powerbi.extensibility.visual.VisualUpdateOptions;
import IVisual = powerbi.extensibility.visual.IVisual;
import IVisualHost = powerbi.extensibility.visual.IVisualHost;
import "./../style/visual.less";
import VisualObjectInstance = powerbi.VisualObjectInstance;
import EnumerateVisualObjectInstancesOptions = powerbi.EnumerateVisualObjectInstancesOptions;
import VisualObjectInstanceEnumerationObject = powerbi.VisualObjectInstanceEnumerationObject;
import { VisualSettings, marginSettings } from "./settings";

import * as React from "react";
import * as ReactDOM from "react-dom";
import { hostViewModel, DataPoint } from "./viewModel"
import Chart from "./chart"
import logExceptions from "./Debug"

/**
 * Function that converts queried data into a view model that will be used by the visual
 *
 * @function
 * @param {VisualUpdateOptions} options - Contains references to the size of the container
 *                                        and the dataView which contains all the data
 *                                        the visual had queried.
 * @param {IVisualHost} host            - Contains references to the host which contains services
 */
function visualTransform(options: VisualUpdateOptions, host: IVisualHost): hostViewModel {
    let dataViews = options.dataViews;
    let viewModel: hostViewModel = {
        dataPoints: [],
        nbTresholds: null
    };
    if (!dataViews
        || !dataViews[0]
        || !dataViews[0].table
        || !dataViews[0].table.columns
        || !dataViews[0].table.rows
        || !dataViews[0].table.rows[0])
        return viewModel;

    const table = options.dataViews[0].table;
    const rows = table.rows;
    let columns = table.columns.map((val, idx) => {

        return {
            displayName: val.displayName,
            type: Object.keys(val.roles)[0],
            rowIndex: idx,
            temporal: val.type.temporal
        }
    })

    let measure = columns.filter(column => column.type == "measure");
    let categories = columns.filter(column => column.type == "category");
    let label = columns.filter(column => column.type == "label");
    let tresholds = columns.filter(column => column.type == "treshold");

    let chartDataPoint: DataPoint[] = [];
    for (let i = 0, len = rows.length; i < len; i++) {
        //@ts-ignore
        let row = rows[i];
        let category = categories[0]
        let tmpDataPoints = {
            categoryName: categories[0].displayName,
            //@ts-ignore
            categoryValue: category.temporal ? new Date(row[category.rowIndex]) : row[category.rowIndex],
            value: row[measure[0].rowIndex],
            valueName: measure[0].displayName,
            label: row[label[0].rowIndex]
        }

        tresholds.forEach((val, idx) => {
            tmpDataPoints[`treshold${idx}`] = {
                name: val.displayName,
                objectName: `treshold${idx + 1}`,
                value: row[tresholds[idx].rowIndex],                
            }
        });
        //@ts-ignore
        chartDataPoint.push(tmpDataPoints);
    }
    return {
        dataPoints: chartDataPoint,
        nbTresholds: tresholds.length
    };
}


export class Visual implements IVisual {

    private target: HTMLElement;
    private reactRoot: React.ComponentElement<any, any>;
    private host: IVisualHost;
    private settings: VisualSettings;

    constructor(options: VisualConstructorOptions) {
        this.reactRoot = React.createElement(Chart, {});
        this.target = options.element;
        this.host = options.host;
        ReactDOM.render(this.reactRoot, this.target);
    }
    @logExceptions()
    public update(options: VisualUpdateOptions) {
        let viewModel: hostViewModel = visualTransform(options, this.host)
        if (options.dataViews && options.dataViews[0]) {
            const dataView: DataView = options.dataViews[0];
            const { width, height } = options.viewport;
            this.settings = VisualSettings.parse(dataView) as VisualSettings;
            const margins = {
                top: this.settings.margins.top,
                bottom: this.settings.margins.bottom,
                right: this.settings.margins.right,
                left: this.settings.margins.left
            }
            const tresholdColors = {
                treshold1: this.settings.tresholds.fillTreshold1,
                treshold2: this.settings.tresholds.fillTreshold2,
                treshold3: this.settings.tresholds.fillTreshold3,
                treshold4: this.settings.tresholds.fillTreshold4
            }
            const modebarDisplay = {
                modeBar: this.settings.modeBar.showBar,
                options: this.settings.modeBar.options,
                optionLabel : this.settings.modeBar.optionLabel

            }
            const settings = {
            	width: width,
            	height: height,
            	viewModel: viewModel,
            	barColor: this.settings.barFormat.fillColor,
            	margins: margins,
            	showLegend: this.settings.legend.showLegend,
            	modeBar: this.settings.modeBar.showBar,
            	showTitle: this.settings.plotTitle.showTitle,
                tresholdLineWidth: this.settings.tresholds.width,
                tresholdColors: tresholdColors,
                modebarDisplay: modebarDisplay
            }
            Chart.update(settings);
        }
    }

    public enumerateObjectInstances(
        options: EnumerateVisualObjectInstancesOptions
    ): VisualObjectInstance[] | VisualObjectInstanceEnumerationObject {
        return VisualSettings.enumerateObjectInstances(this.settings || VisualSettings.getDefault(), options);
    }
}

