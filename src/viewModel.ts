/**
 * Interface pour le viewModel du visuel.
 *
 * @interface
 * @property {BarChartDataPoint[]} dataPoints - Points de données utilisées pour restituer le visuel.
 * @property {number} nbTresholds                 - Nombre de seuils utilisés par le visuel.
 */
export interface hostViewModel {
    dataPoints: DataPoint[];
    nbTresholds: number;
  };
  
  /**
   * Interface pour un point de données.
   */
  export interface DataPoint {
    categoryName: string;
    categoryValue: any;
    valueName: string;
    value: number;
    label: string;
  };
  
export default hostViewModel