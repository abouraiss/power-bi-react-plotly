"use strict";

 import { dataViewObjectsParser } from "powerbi-visuals-utils-dataviewutils";
 import DataViewObjectsParser = dataViewObjectsParser.DataViewObjectsParser;

//  export class CircleSettings {
//      public circleColor: string = "white";
//      public showAllDataPoints: boolean = true;
//  }

 export class VisualSettings extends DataViewObjectsParser {
    public plotTitle: titleSettings = new titleSettings();
    public legend: legendSettings = new legendSettings();
    public tresholds: tresholdSettings = new tresholdSettings();
    public barFormat: barSettings = new barSettings();
    public modeBar: modeBarSettings = new modeBarSettings();
    public margins: marginSettings = new marginSettings();

 }

 export class titleSettings {
    public showTitle: boolean = true;
  }

  export class legendSettings {
    public showLegend: boolean = true;
  }

  export class tresholdSettings {
    public width: number = 2;
    public fillTreshold1:  string = "00429d";
    public fillTreshold2:  string = "73a2c6";
    public fillTreshold3:  string = "f4777f";
    public fillTreshold4:  string = "93003a";
  }
  
  export class barSettings {
    public fillColor: string = "7DBE0A";
  }

  export class modeBarSettings {
    public showBar: boolean= true;
    public options: string= null;
    public optionLabel: string="Naviguer"
  }

  export class marginSettings {
    public top: number = 25;
    public bottom: number = 55;
    public right: number = 25;
    public left: number = 45;
  }

 