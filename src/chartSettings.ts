import {
	hostViewModel,
} from "./viewModel"

export interface State {
    width: number,
      height: number,
      viewModel: hostViewModel,
      barColor: string,
      tresholdLineWidth: number,
      tresholdColors: {
        treshold1: string,
        treshold2: string,
        treshold3: string,
        treshold4: string,
      }
    showLegend: boolean,
      showTitle: boolean,
      modebarDisplay: {
        modeBar: boolean,
        options: string,
        optionLabel: string
      },
      margins: {
        top: number,
        bottom: number,
        right: number,
        left: number,
      }
  }

  export const initialState: State = {
	width: null,
	height: null,
	viewModel: null,
	barColor: null,
	tresholdLineWidth: null,
	tresholdColors: {
		treshold1: null,
		treshold2: null,
		treshold3: null,
		treshold4: null,
	},
	showLegend: true,
	showTitle: true,
	modebarDisplay: {
		modeBar: null,
		options: null,
		optionLabel: null
	},
	margins: {
		top: null,
		bottom: null,
		right: null,
		left: null,
	}
}

export const tickFormat = [{
	dtickrange: [3000, 60000],
	value: "%H:%M"
}, {
	dtickrange: [null, 3000],
	value: "%H:%M"
}]

export const removeList = ["toImage", "zoom2d", "pan2d", "select2d", "lasso2d", "zoomIn2d", "zoomOut2d", "autoScale2d", "resetScale2d", "hoverClosestGl2d", "hoverClosestPie", "toggleHover", "resetViews", "toImage", "sendDataToCloud", "toggleSpikelines", "resetViewMapbox", "hoverClosestCartesian", "hoverCompareCartesian"]


