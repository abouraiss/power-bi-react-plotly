import * as d3 from "d3";
import * as React from "react";
import createPlotlyComponent from 'react-plotly.js/factory';
import Plotly from 'plotly.js-basic-dist'

import {
	State,
	initialState,
	tickFormat,
	removeList
} from "./chartSettings"

import {
	DataPoint
} from "./viewModel"

import frenchTranslation from "./locale"

const Plot = createPlotlyComponent(Plotly);
let buttonName :string;

export class Chart extends React.Component < {} > {
		public state: State = initialState;
		private dragVal = false;
		private static updateCallback: (data: object) => void = null;

		constructor(props: any) {
			super(props);
			this.state = initialState;
			this.frenchLocale()
		}		

		public static update(newState: State) {
			if (typeof Chart.updateCallback === 'function') {
				Chart.updateCallback(newState);
			}
		}

		public componentWillMount() {
			Chart.updateCallback = (newState: State): void => {
				this.setState(newState);
			};
		}

		public componentWillUnmount() {
			Chart.updateCallback = null;
		}

		public buildLayout(seriesArray) {
			let titleSelection = this.state.showTitle == true
			let layoutObject = {
				width: this.state.width,
				height: this.state.height,
				margin: {
					l: this.state.margins.left,
					r: this.state.margins.right,
					b: this.state.margins.bottom,
					t: titleSelection ? this.state.margins.top : 0
				},
				showlegend: this.state.showLegend,
				hovermode: "closest",
				hoverlabel: {
					bgcolor: "#3D3D3D"
				},
				xaxis: {
					tickformatstops: tickFormat,
					tickangle: 0
				},
				dragmode: false
			}
			// Si affichage du titre
			if (titleSelection && seriesArray.length == 1) {
				layoutObject["title"] = {
					font: {
						color: "fill: rgb(0, 0, 0)",
						family: "Arial"
					},
					text: `${this.buildTitle(seriesArray)}`
				}
			}
			// si plus d'un polluant
			if (seriesArray.length > 1) {
				layoutObject["grid"] = {
					rows: 2,
					columns: 1,
					pattern: 'independent'
				}
				if (titleSelection) {
					layoutObject["annotations"] = [{
						x: 0.5,
						y: 1.0,
						font: {
							size: 16
						},
						text: seriesArray[0].key,
						xref: 'paper',
						yref: 'paper',
						xanchor: 'center',
						yanchor: 'bottom',
						showarrow: false
					}, {
						x2: 0.775,
						y2: 1.0,
						font: {
							size: 16
						},
						text: seriesArray[1].key,
						xref: 'paper',
						yref: 'paper',
						xanchor: 'center',
						yanchor: 'top',
						showarrow: false
					}]
				}
				layoutObject["xaxis2"] = {
					tickformatstops: tickFormat,
					tickangle: 0
				}
			}
			return layoutObject
		}

		buildTitle(seriesArray) {
			let titles = seriesArray.map(obj => obj.key)
			return titles.join(", ")
		}

		frenchLocale() {
			Plotly.register(frenchTranslation);
			Plotly.setPlotConfig({
				locale: 'fr'
			})
		}

		tresholds(seriesArray, nbTresholds) {
			let tresholdsArray = []
			seriesArray.forEach((points, idx) => {
				let dataPoints: DataPoint[] = points.values
				let categories = dataPoints.map(point => point.categoryValue)
				for (let i = 0; i < nbTresholds; i++) {
					let tresholdKey = `treshold${i}`
					console.log(dataPoints[0][tresholdKey].objectName)
					//debugger;
					let constant = dataPoints.map(point => point[tresholdKey].value)
					let trace = {
						type: 'scatter',
						x: categories,
						y: constant,
						mode: constant.length == 1 ? 'markers' : "lines",
						marker: {
							symbol: 'square-dot',
						},
						//@ts-ignore
						name: dataPoints[0][tresholdKey].name,
						line: {
							width: this.state.tresholdLineWidth,
							dash: 'dot',
						},
						showlegend: this.state.showLegend,
						legendgroup: points.key,
						hovertemplate: ` %{fullData.name}:  <b> %{y}<b><br><extra></extra>`,
					}
					if (idx == 1) {
						trace["xaxis"] = 'x2',
							trace["yaxis"] = 'y2'
					}
					// Couleur automatique au dela de 4 seuils
					if (i < 5) {
						trace.line["color"] = this.state.tresholdColors[dataPoints[0][tresholdKey].objectName]
					}
					//@ts-ignore
					tresholdsArray.push(trace)
				}
			})
			return tresholdsArray.sort(function(x, y) {
				return d3.ascending(x.name, y.name);
			})
		}

		chartData() {
			// regroupement des séries par polluants
			let series = d3.nest().key(function(d: DataPoint) {
				return d.label;
			}).entries(this.state.viewModel.dataPoints);
			// Ajout des series
			let traces = series.map((points, idx) => {
				let dataPoints: DataPoint[] = points.values
				let categories = dataPoints.map(point => point.categoryValue)
				let values = dataPoints.map(point => point.value)
				let trace = {
					type: 'scatter',
					x: categories,
					y: values,
					marker: {
						color: this.state.barColor
					},
					name: points.key,
					legendgroup: points.key,
					hovertemplate: `<b>%{x}</b><br>${points.key}:  <b> %{y}<b><br><extra></extra>`,
				}
				if (idx == 1) {
					trace["xaxis"] = 'x2',
						trace["yaxis"] = 'y2'
				}
				return trace
			})
			// Ajout des seuils
			let tresholdsArray = this.tresholds(series, this.state.viewModel.nbTresholds)
			buttonName = this.state.modebarDisplay.optionLabel;
			let buttonConfig = [{
				name: buttonName,
				icon: Plotly.Icons.pan,
				direction: 'up',
				click: this.modebarRelayout
			}]
			// Mise en forme
			let layout = this.buildLayout(series);
			var config = {
				locale: 'fr',
				displaylogo: false,
				responsive: true,
				scrollZoom: true,
				modeBarButtonsToAdd: buttonConfig
			}
			// Parametrage de la barre d'options
			// zoom2d, pan2d, select2d, lasso2d, zoomIn2d, zoomOut2d, autoScale2d, resetScale2d
			let displayedoptions = this.state.modebarDisplay.options ? this.state.modebarDisplay.options.split(",") : null
			if (this.state.modebarDisplay.modeBar == true) {
				config["modeBarButtonsToRemove"] = removeList
				if (displayedoptions) {
					//@ts-ignore
					config["modeBarButtonsToRemove"] = removeList.filter(option => !displayedoptions.includes(option))
				}
			} else {
				config["displayModeBar"] = false
			}
			return [traces.concat(tresholdsArray), layout, config]
		}

		modebarRelayout(gd) {
			this.dragVal = !this.dragVal
			let button = document.querySelector(`.modebar-btn[data-title="${buttonName}"] > svg > path`)
			if (this.dragVal) {
				//@ts-ignore
				button.style.setProperty('fill', 'rgba(68, 68, 68, 0.7)', 'important');
				console.log(button)
			} else {
				//@ts-ignore
				button.style.setProperty("fill", "rgba(68, 68, 68, 0.3)", 'important')
			}
			Plotly.relayout(gd, {
				dragmode: this.dragVal
			})
		}

		update(props) {
			//@ts-ignore
			this.state.layout.width = props.width
			this.state.tresholdLineWidth = props.tresholdLineWidth
			this.state.tresholdColors = props.tresholdColors
			this.state.showLegend = props.showLegend
			this.state.showTitle = props.showTitle
			this.state.viewModel = props.viewModel
			this.state.barColor = props.barColor
			this.state.modebarDisplay = props.modeBarDisplay
			this.state.margins = props.margins
		}

		render() {
			let data = {};
			let config = {};
			let layout = {};
			if (this.state.viewModel) {
				[data, layout, config] = this.chartData()
			}
			return ( < Plot data = {data}
				layout = {layout}
				config = {config}
				/>);
			}
		}
		
		export default Chart;